import "dotenv/config";

export default {
    swaggerDefinition: {
        info: {
            description: "API documentation for FiveRP LSPD Panel",
            title: "FiveRP LSPD ",
            version: "0.1.0",
        },
        host: process.env.HOST + ":" + process.env.PORT,
        basePath: "",
        produces: [
            "application/json"
        ],
        consumes: [
            "application/json"
        ],
        schemes: ["http", "https"],
        securityDefinitions: {
            JWT: {
                type: "apiKey",
                in: "header",
                required: true,
                name: "Authorization",
                description: ""
            }
        }
    },
    basedir: __dirname, //app absolute path
    files: ["./routes/**/*.js", "./controllers/**/*.js", "./models/**/*.js"] //Path to the API handle folder
};