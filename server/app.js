"use strict";
import "dotenv/config";
import router from "./routes/index";
import express from "express";
import expressSwaggerLib from "express-swagger-generator";
import expressSanitizer from "express-sanitizer";
import logger from "morgan";
import swaggerConf from "./api-docs";
import cors from "cors";
import {isApplicationReady} from "./middlewares/appReady";

const app = express();

/**
 * Swagger pages start
 */
const expressSwagger = expressSwaggerLib(app);
expressSwagger(swaggerConf);

app.use(logger("dev"));
app.use(cors());
app.use(express.json({type: "application/json", strict: true}));
app.use(express.urlencoded({ extended: false }));
app.use(expressSanitizer());

app.use("/", isApplicationReady, router);

export default app;
