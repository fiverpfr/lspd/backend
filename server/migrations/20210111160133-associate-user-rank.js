"use strict";

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable(
            "user_ranks",
            {
                createdAt: {
                    allowNull: false,
                    type: Sequelize.DATE,
                },
                updatedAt: {
                    allowNull: false,
                    type: Sequelize.DATE,
                },
                userId: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    references: {
                        model: "users",
                        key: "id"
                    },
                    onUpdate: "CASCADE"
                },
                rankId: {
                    type: Sequelize.STRING,
                    primaryKey: true,
                    references: {
                        model: "ranks",
                        key: "idRank"
                    },
                    onUpdate: "CASCADE"
                },
            }
        );
    },

    down: async (queryInterface) => {
        return queryInterface.dropTable("user_ranks");
    }
};
