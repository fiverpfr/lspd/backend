'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('user_ranks', 'primary', {
      type: Sequelize.DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false
    })
  },

  down: async (queryInterface) => {
    await queryInterface.removeColumn("user_ranks", 'primary');
  }
};
