'use strict';

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.sequelize.query(`
      CREATE OR REPLACE FUNCTION purge_user_ranks()
      RETURNS trigger AS
      $func$
      BEGIN
          DELETE FROM user_ranks ur
          WHERE ur."rankId" = OLD."rankId";
          return OLD;
      end;
      $func$ LANGUAGE plpgsql;
      
      DROP trigger if exists ranks_purge_user_ranks ON ranks;
      
      create trigger ranks_purge_user_ranks
          before delete
          on ranks
          for each row
              execute procedure purge_user_ranks();
    `);
  },

  down: async (queryInterface) => {
    await queryInterface.sequelize.query('DROP trigger if exists ranks_purge_user_ranks ON ranks;')
  }
};
