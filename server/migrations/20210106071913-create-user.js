"use strict";

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable("users", {
            id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                unique: true
            },
            social_id: {
                type: Sequelize.BIGINT,
                allowNull: false
            },
            first_name: {
                type: Sequelize.STRING,
                allowNull: false,
                validate: {
                    max: 18,
                    min: 3
                }
            },
            last_name: {
                type: Sequelize.STRING,
                allowNull: false,
                validate: {
                    max: 18,
                    min: 3
                }
            },
            password: {
                type: Sequelize.STRING,
                allowNull: false
            },
            active: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: async (queryInterface) => {
        await queryInterface.dropTable("users");
    }
};