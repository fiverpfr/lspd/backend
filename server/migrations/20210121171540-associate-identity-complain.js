"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
        "complain_identities",
        {
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          complain_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            references: {
              model: "complains",
              key: "complain_id"
            },
            onUpdate: "CASCADE",
            allowNull: false
          },
          identity_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            references: {
              model: "identities",
              key: "identity_id"
            },
            onUpdate: "CASCADE",
            allowNull: false
          },
          type: {
            type: Sequelize.ENUM("complainer", "target"),
            allowNull: false
          },
        }
    );
  },

  down: async (queryInterface) => {
    return queryInterface.dropTable("complain_identities");
  }
};
