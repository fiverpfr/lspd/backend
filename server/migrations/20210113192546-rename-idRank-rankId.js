'use strict';

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.renameColumn("ranks","idRank", "rankId");
  },

  down: async (queryInterface) => {
    await queryInterface.renameColumn("ranks","rankId", "idRank");
  }
};
