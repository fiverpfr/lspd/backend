'use strict';

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.sequelize.query(`
      CREATE OR REPLACE FUNCTION purge_identity_complains()
          RETURNS trigger AS
      $func$
      BEGIN
          DELETE FROM complain_identities ci
          WHERE ci.identity_id = OLD.identity_id;
          return OLD;
      end;
      $func$ LANGUAGE plpgsql;
      
      DROP trigger if exists identities_purge_complains ON identities;
      
      create trigger identities_purge_complains
          before delete
          on identities
          for each row
              execute procedure purge_identity_complains();
    `);
  },

  down: async (queryInterface) => {
    await queryInterface.sequelize.query('DROP trigger if exists identities_purge_complains ON identities;');
  }
};
