'use strict';

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.sequelize.query(`
      CREATE OR REPLACE FUNCTION purge_complain_identities()
          RETURNS trigger AS
      $func$
      BEGIN
          DELETE FROM complain_identities ci
          WHERE ci.complain_id = OLD.complain_id;
          return OLD;
      end;
      $func$ LANGUAGE plpgsql;
      
      DROP trigger if exists complains_purge_identities ON identities;
      
      create trigger complains_purge_identities
          before delete
          on complains
          for each row
              execute procedure purge_complain_identities();
    `);
  },

  down: async (queryInterface) => {
    await queryInterface.sequelize.query('DROP trigger if exists complains_purge_identities ON complains;');
  }
};
