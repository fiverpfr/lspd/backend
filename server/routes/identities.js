import express from "express";
import identityController from "../controllers/identityController";
import {isRequestIdValid} from "../middlewares/ids";
import parseSearchParams from "../middlewares/search";
import {guard} from "../middlewares/guard";

const app = express();

/**
 * Fetch all identities
 * @route GET /identities
 * @group IdentityController - Operations about identities
 * @returns {Array<identity>} 200 - An array of identity info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.get("/", identityController.getAll);

/**
 * Search in all identities
 * @route GET /identities/search
 * @group IdentityController - Operations about identities
 * @param {string} q.query.required - Query of search
 * @param {string} limit.query - Limit of result
 * @param {string} offset.query - Offset
 * @returns {Array<identity>} 200 - An array of identity info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.get("/search", parseSearchParams, identityController.search);

/**
 * Get specific identity data
 * @route GET /identities/{identity_id}
 * @group IdentityController - Operations about identity
 * @param {string} identity_id.path.required - Id of target identity
 * @returns {identity.model} 200 - An array of identity info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.get("/:id", isRequestIdValid, identityController.get);

/**
 * Get complains linked to identity
 * @route GET /identities/{identity_id}/complains
 * @group IdentityController - Operations about identity
 * @param {string} identity_id.path.required - Id of target identity
 * @returns {Array<complain>} 200 - An array of complain info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.get("/:id/complains", isRequestIdValid, identityController.complains);

/**
 * Create new identity
 * @route POST /identities
 * @group IdentityController - Operations about identities
 * @param {identity.model} identity.body.required - Model of brand new identity
 * @returns {identity.model} 200 - An array of identity info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.post("/", identityController.create);

/**
 * Delete a identity
 * @route DELETE /identities/{identity_id}
 * @group IdentityController - Operations about identities
 * @param {string} identity_id.path.required - Id of target identity
 * @returns {identity.model} 200 - Array of identity info of deleted identity
 * @returns {Error} 404 - identity not found
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.delete("/:id", guard.requireAny("identity.delete", "*"), isRequestIdValid, identityController.remove);

/**
 * Update identity
 * @route PUT /identities/{identity_id}
 * @group IdentityController - Operations about identity
 * @param {number} identity_id.path.required - Id of target identity
 * @returns {identity.model} 200 - Array of identity info with new updated data
 * @returns {Error} 404 - identity not found
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.put("/:id", guard.requireAny("identity.update", "*"), isRequestIdValid, identityController.update);


export default app;
