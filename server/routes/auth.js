import express from "express";
import authController from "../controllers/authController";
import {validateLoginFields} from "../middlewares/authJwt";

const app = express();

/**
 * Authenticate user
 * @route POST /auth/login
 * @group authController - Operations about authentication
 * @param {string} username.formData.required - Username - eg: John_Doe
 * @param {string} password.formData.required - Password
 * @consumes application/x-www-form-urlencoded
 * @returns {string} 200 - Return JWS
 * @returns {Error} 400 - Fields username or password are not found
 * @returns {Error} 404 - Wrong username / password
 */
app.post("/login", validateLoginFields, authController.login);

export default app;
