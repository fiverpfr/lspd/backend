import express from "express";
import rankController from "../controllers/rankController";
import {guard} from "../middlewares/guard";

const app = express();

/**
 * This function comment is parsed by doctrine
 * @route GET /ranks
 * @group RankController - Operations about rank
 * @returns {Array<rank>} 200 - An array of rank info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.get("/", guard.requireAny("rank.read", "*"), rankController.getAll);

/**
 * Get specific rank data
 * @route GET /ranks/{rankId}
 * @group RankController - Operations about rank
 * @param {string} rankId.path.required - Id of target rank
 * @returns {rank.model} 200 - An array of rank info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.get("/:id", guard.requireAny("rank.read", "*"), rankController.get);

/**
 * Create new rank
 * @route POST /ranks
 * @group RankController - Operations about rank
 * @param {rank.model} rank.body.required - Model of brand new rank
 * @returns {rank.model} 200 - An array of rank info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.post("/", guard.requireAny("rank.create", "*"), rankController.create);

/**
 * Delete a rank
 * @route DELETE /ranks/{rankId}
 * @group RankController - Operations about rank
 * @param {string} rankId.path.required - Id of target rank
 * @returns {rank.model} 200 - Array of rank info of deleted rank
 * @returns {Error} 404 - Rank not found
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.delete("/:id", guard.requireAny("rank.delete", "*"), rankController.remove);

/**
 * Update rank
 * @route PUT /ranks/{rankId}
 * @group RankController - Operations about rank
 * @param {number} rankId.path.required - Id of target rank
 * @returns {rank.model} 200 - Array of rank info with new updated data
 * @returns {Error} 404 - Rank not found
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.put("/:id", guard.requireAny("rank.update", "*"), rankController.update);


export default app;
