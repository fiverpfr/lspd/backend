import express from "express";
import complainController from "../controllers/complainController";
import {isRequestIdValid} from "../middlewares/ids";
import parseSearchParams from "../middlewares/search";
import {guard} from "../middlewares/guard";

const app = express();

/**
 * Fetch all complains
 * @route GET /complains
 * @group ComplainController - Operations about complains
 * @returns {Array<complain>} 200 - An array of complain info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.get("/", complainController.getAll);

/**
 * Search in all complains
 * @route GET /complains/search
 * @group ComplainController - Operations about complain
 * @param {string} q.query.required - Query of search
 * @param {string} limit.query - Limit of result
 * @param {string} offset.query - Offset
 * @returns {Array<identity>} 200 - An array of identity info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.get("/search", parseSearchParams, complainController.search);

/**
 * Get specific rank data
 * @route GET /complains/{complain_id}
 * @group ComplainController - Operations about complain
 * @param {string} complain_id.path.required - Id of target complain
 * @returns {complain.model} 200 - An array of complain info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.get("/:id", isRequestIdValid, complainController.get);

/**
 * Get specific complain identities
 * @route GET /complains/{complain_id}/identities
 * @group ComplainController - Operations about complain
 * @param {string} complain_id.path.required - Id of target complain
 * @returns {Array<identity>} 200 - An array of identities info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.get("/:id/identities", isRequestIdValid, complainController.identities);

/**
 * Create new complain
 * @route POST /complains
 * @group ComplainController - Operations about complains
 * @param {complain.model} complain.body.required - Model of brand new complain
 * @returns {complain.model} 200 - An array of complain info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.post("/", complainController.create);

/**
 * Delete a complain
 * @route DELETE /complains/{complain_id}
 * @group ComplainController - Operations about complains
 * @param {string} complain_id.path.required - Id of target complain
 * @returns {complain.model} 200 - Array of complain info of deleted complain
 * @returns {Error} 404 - complain not found
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.delete("/:id", guard.requireAny("complain.delete", "*"), isRequestIdValid, complainController.remove);

/**
 * Update complain
 * @route PUT /complains/{complain_id}
 * @group ComplainController - Operations about complain
 * @param {number} complain_id.path.required - Id of target complain
 * @returns {complain.model} 200 - Array of complain info with new updated data
 * @returns {Error} 404 - complain not found
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.put("/:id", isRequestIdValid, complainController.update);


export default app;
