import express from "express";
import "dotenv/config";
import {isActiveUser, verifyToken} from "../middlewares/authJwt";
import authRouter from "./auth";
import usersRouter from "./users";
import ranksRouter from "./ranks";
import identitiesRouter from "./identities";
import complainsRouter from "./complains";
import miscController from "../controllers/miscController";
import {hasAnyRank} from "../middlewares/rank";
import {errorHandler} from "../middlewares/errorHandler";

const Router = express.Router();

/**
 * Health check
 * @route GET /health
 * @group miscController - Misc routes
 * @returns {Error}  503 - Service not ready
 * @returns {healthStructure.model} 200 - Application ready & health state
 */
Router.get("/health", miscController.healthCheck);

/**
 * Home route
 * @route GET /
 * @group miscController - Misc routes
 * @returns {JSON} 200 - Home page
 */
Router.get("/", miscController.home);

/**
 * Authentication router
 */
Router.use("/auth", authRouter);

/**
 * Verify if the token is valid & has an correct userid
 */
Router.use(verifyToken);

/**
 * If user is authenticated and got desactivated, we stop him
 */
Router.use(isActiveUser);

/**
 * If no rank given to an user, we stop him
 */
Router.use(hasAnyRank);

Router.use("/users", usersRouter);
Router.use("/ranks", ranksRouter);
Router.use("/identities", identitiesRouter);
Router.use("/complains", complainsRouter);


Router.use(errorHandler);
Router.use((req, res) => {
    res.status(404)
        .json({error: "Route not found"})
        .end();
});

export default Router;