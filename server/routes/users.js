import express from "express";
import userController from "../controllers/userController";
import {isRequestIdValid, targetUserExists, targetUserExistsAndInactive} from "../middlewares/ids";
import parseSearchParams, {parsePermission} from "../middlewares/search";
import {guard} from "../middlewares/guard";
import {targetRankExists} from "../middlewares/rank";

const app = express();

/**
 * Fetch all users
 * @route GET /users
 * @group UserController - Operations about user
 * @returns {Array<user>} 200 - An array of user info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.get("/", parseSearchParams, guard.requireAny("user.list", "*"), userController.getAll);

/**
 * Fetch the count of all users
 * @route GET /users/count
 * @group UserController - Operations about user
 * @returns {integer} 200 - Number of users
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.get("/count", guard.requireAny("user.list", "*"), parseSearchParams, userController.getAllCount);

/**
 * Retrieve the current user based on his token
 * @route GET /users/me
 * @group MeController - Operations about the logged user
 * @returns {user.model} 200 - Array of user with his infos
 * @returns {Error} 404 - User not found
 * @returns {Error} 401 - No authorization
 * @security JWT
 */
app.get("/me", userController.me);

/**
 * Retrieve the current user complains
 * @route GET /users/me/complains
 * @group MeController - Operations about the logged user
 * @returns {Array<complain>} 200 - Array of complains with his infos
 * @returns {Error} 404 - User not found
 * @returns {Error} 401 - No authorization
 * @security JWT
 */
app.get("/me/complains", parseSearchParams, userController.me_complains);

/**
 * Returns if the user can do the action specified
 * @route GET /users/me/can
 * @group MeController - Operations about the logged user
 * @returns {boolean} 200 - Boolean if the user can access the permission specified
 * @param {string} perm.query.required - Name of permission
 * @returns {Error} 404 - User not found
 * @returns {Error} 401 - No authorization
 * @security JWT
 */
app.get("/me/can", parsePermission, userController.me_can);

/**
 * Fetch specific user data
 * @route GET /users/{userId}
 * @group UserController - Operations about user
 * @param {number} userId.path.required - Id of target user
 * @returns {user.model} 200 - An array of user info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.get("/:id", isRequestIdValid, guard.requireAny("user.list", "*"), userController.get);

/**
 * Create new user, requires the user to have user.create permission
 * @route POST /users
 * @group UserController - Operations about user
 * @param {user.model} user.body.required - Model of brand new user
 * @returns {user.model} 200 - An array of user info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.post("/", guard.requireAny("user.create", "*"), userController.create);

/**
 * Add a rank to the specified user
 * @route POST /users/{userId}/rank
 * @group UserController - Operations about user
 * @param {number} userId.path.required - Id of target user
 * @param {user_rank.model} rank.body.required - Model of target rank to add
 * @returns {user_rank.model} 200 - An array of user info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.post("/:id/rank", isRequestIdValid, targetUserExists, guard.requireAny("user_rank.create", "*"), userController.setRank);

/**
 * Remove a rank to the specified user
 * @route POST /users/{userId}/rank/{rankId}
 * @group UserController - Operations about user
 * @param {number} userId.path.required - Id of target user
 * @param {number} rankId.path.required - Id of target user
 * @returns {user_rank.model} 200 - An array of user info
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.delete("/:id/rank/:rankId", isRequestIdValid, targetUserExists, targetRankExists, guard.requireAny("user_rank.delete", "*"), userController.removeRank);

/**
 * Delete a user, requires the user to have user.delete permission (only admin has)
 * @route DELETE /users/{userId}
 * @group UserController - Operations about user
 * @param {number} userId.path.required - Id of target user
 * @returns {user.model} 200 - Array of user info of deleted user
 * @returns {Error} 404 - User not found
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.delete("/:id", isRequestIdValid, targetUserExists, guard.requireAny("user.delete", "*"), userController.remove);

/**
 * Update user, requires the user to have user.update permission
 * @route PUT /users/{userId}
 * @group UserController - Operations about user
 * @param {number} userId.path.required - Id of target user
 * @returns {user.model} 200 - Array of user info with new updated data
 * @returns {Error} 404 - User not found
 * @returns {Error} 401 - No authorization
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
app.put("/:id", isRequestIdValid, targetUserExistsAndInactive, guard.requireAny("user.update", "*"), userController.update);

export default app;
