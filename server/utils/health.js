import sequelize from "../sequelize";

export function applicationHealthy() {
    /**
     * @returns {{databaseIsOk: boolean, healthy: boolean, uptime: *}} - complete health state of appliction
     */
    const structure = {
        uptime: process.uptime(),
        healthy: false,
        databaseIsOk: false
    };

    if (sequelize.connected)
        structure.databaseIsOk = true;

    if (structure.databaseIsOk) // will add further parameter to check later
        structure.healthy = true;

    return structure;
}