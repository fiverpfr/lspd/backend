import logger from "../bin/logger";
import {identity} from "../models/identity";
import {Op} from "sequelize";
import {complain} from "../models/complain";

class IdentityController {
    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    get(req, res) {
        let uid = req.params.id;

        identity.findByPk(uid)
            .then((result) => {
                if (result != null)
                    res.send({success: true, data: result})
                        .end();
                else
                    res.status(404)
                        .send({success: false, message: `${identity.name} not found`})
                        .end();
            })
            .catch(err => {
                logger.error("identityController:get, failed to fetch identity");
                logger.error(`identityController:get, ${err}`);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }

    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    search(req, res) {
        identity.findAndCountAll({
            where: {
                [Op.or]: [
                    {first_name: {
                        [Op.iLike]: `%${req.search.query}%`
                    }},
                    {last_name: {
                        [Op.iLike]: `%${req.search.query}%`
                    }},
                    {phone_number: {
                        [Op.iLike]: `%${req.search.query}%`
                    }}
                ]
            },
            limit: req.search.limit || 15,
            offset: req.search.offset || 0
        })
            .then((result) => {
                if (result != null)
                    res.send({success: true, data: result})
                        .end();
                else
                    res.status(404)
                        .send({success: false, message: `${identity.name} not found`})
                        .end();
            })
            .catch(err => {
                logger.error("identityController:search, failed to search identities");
                logger.error(`identityController:search, ${err}`);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }

    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    complains(req, res) {
        let identity_id = parseInt(req.params.id);

        complain.findAll({
            include: ["identities"],
            limit: 15
        })
            .then(result => result.filter(c => c.identities.length > 0))
            // Find only complains where the identity is related to
            .then(result => result.filter(c => c.identities.find(p => p.identity_id === identity_id)))
            // Format to have the count & rows
            .then(result => ({count: result.length, rows: result}))
            .then(result => {
                res.send({success: true, data: result})
                    .end();
            })
            .catch(err => {
                logger.error("identityController:complains, failed to fetch identity complains");
                logger.error(`identityController:complains, ${err}`);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }

    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    getAll(req, res) {
        identity.findAndCountAll()
            .then(function (result) {
                res.send({success: true, data: result})
                    .end();
            })
            .catch(err => {
                logger.error("identityController:getAll, failed to fetch identities");
                logger.error(`identityController:getAll, ${err}`);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }

    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    async update(req, res) {
        let uid = req.params.id;
        let body = req.body;

        identity.findByPk(uid)
            .then((result) => {
                if (result === null) {
                    res.status(404)
                        .send({success: false, message: `${identity.name} not found`})
                        .end();
                    return;
                }

                result.update(body)
                    .then(updated => {
                        res.status(200)
                            .send({success: true, data: updated})
                            .end();
                    })
                    .catch(err => {
                        logger.error("identityController:update, failed to update identity");
                        logger.error(`identityController:update, ${err}`);
                        res.status(400)
                            .send({success: false, message: `Request error, ${err}`})
                            .end();
                    });

            })
            .catch(err => {
                logger.error("identityController:update, failed to fetch identity");
                logger.error(`identityController:update, ${err}`);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }

    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    async create(req, res) {
        let body = req.body;

        identity.create(body).then(identityData => {
            logger.info(`identityController:create, new ${identityData.identity_id}`);
            return res.status(201)
                .json({success: true, data: identityData})
                .end();
        }).catch(err => {
            logger.error("identityController:create, failed to create identity");
            logger.error(`identityController:create, ${err}`);

            return res.status(400)
                .json({success: false, message: err})
                .end();
        });
    }

    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    remove(req, res) {
        identity.findByPk(req.params.id).then(async result => {
            if (result == null) {
                return res.status(404)
                    .json({success: false, message: `Target ${identity.name} not found`})
                    .end();
            }

            logger.info(`identityController:remove, remove ${result.identity_id}`);
            await result.destroy();
            res.status(200)
                .json({success: true, data: result})
                .end();
        }).catch(err => {
            logger.error("identityController:remove, failed to remove identity");
            logger.error(`identityController:remove, ${err}`);
            return res.status(500)
                .json({success: false, message: `Error while trying to fetch ${identity.name}, ${err}`})
                .end();
        });

    }
}

export default new IdentityController();