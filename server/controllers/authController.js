import logger from "../bin/logger";
import {user} from "../models/user";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import {Sequelize} from "sequelize";
import "dotenv/config";

class authController {
    async login(req, res) {
        function noMatch() {
            return res.status(401)
                .json({success: false, message: "Username or password not found"})
                .end();
        }

        logger.debug("authController:login, attempt to log to user " + req.body.username);

        let [ first_name, last_name ] = req.body.username.split("_");
        let password = req.body.password;
        let user_data = await user.findActiveUserWithPassword({
            where: {last_name: {[Sequelize.Op.iLike]: last_name}, first_name: {[Sequelize.Op.iLike]: first_name}},
            include: ["ranks"]
        });

        if (user_data === null) return noMatch();
        if (!user_data.primary_rank)
        {
            logger.warn("authController:login, cannot log as user has no rank");
            return noMatch();
        }

        if (!await bcrypt.compare(password, user_data.password)) return noMatch();

        let payload = {
            iat: Math.floor(Date.now() / 1000),
            first_name: user_data.first_name,
            last_name: user_data.last_name,
            uid: user_data.id
        };

        let options = {
            expiresIn: process.env.JWT_EXPIRE || "15d",
            issuer: user_data.username
        };

        jwt.sign(payload, process.env.JWT_SECRET, options, (err, response) => {
            if (err) {
                logger.error("authController:login, failed to generate JWT");
                logger.error(`authController:login, ${err}`);
                res.status(500)
                    .json({success: false, message: err})
                    .end();
            }

            logger.debug("authController:login, logged in successfully for user " + req.body.username);
            res.send({success: true, data: response}).end();
        });
    }
}

export default new authController();