import {applicationHealthy} from "../utils/health";
import "dotenv/config";

/**
 * @typedef healthStructure
 * @property {integer} uptime - Uptime of service
 * @property {boolean} healthy - Is application healthy
 * @property {boolean} databaseIsOk - Is database connected properly
 */

class miscController {

    /**
	 * Home route
	 *
	 * @param {Request} req
	 * @param {ResponseType} res
	 */
    home(req, res) {
        if (process.env.NODE_ENV === "development") {
            res.json({success:true, message: "Welcome to backend API. See GET /api-docs to see routes"}).end();
            return;
        }

        res.json({success: true, message: "Welcome to backend API"}).end();
    }

    /**
	 * Check health of the current service and its dependencies
	 *
	 * @param {Request} req
	 * @param {ResponseType} res
	 */
    healthCheck(req, res) {
        let structure = applicationHealthy();

        if (structure.healthy)
            res.json({success: true, data: structure}).end();
        else
            res.status(503).json({success: false, data: structure}).end();
    }
}

export default new miscController();