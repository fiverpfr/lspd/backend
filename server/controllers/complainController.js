import logger from "../bin/logger";
import {complain} from "../models/complain";
import {complain_identities} from "../models/complain_identity";
import {Op} from "sequelize";

class ComplainController {
    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    get(req, res) {
        let uid = req.params.id;

        complain.findByPk(uid)
            .then((result) => {
                if (result != null)
                    res.send({success: true, data: result})
                        .end();
                else
                    res.status(404)
                        .send({success: false, message: `${complain.name} not found`})
                        .end();
            })
            .catch(err => {
                logger.error("complainController:get, failed to fetch complain");
                logger.error(`complainController:get, ${err}`);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }

    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    getAll(req, res) {
        let options = {};
        if (req.body.options !== undefined)
            options = req.body.options;

        complain.findAndCountAll(options)
            .then(function (result) {
                res.send({success: true, data: result})
                    .end();
            })
            .catch(err => {
                logger.error("complainController:getAll, failed to fetch complains");
                logger.error(`complainController:getAll, ${err}`);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }

    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    identities(req, res) {
        let complain_id = parseInt(req.params.id);

        complain.findByPk(complain_id)
            .then((result) => {
                if (result != null)
                    return true;
                else
                    res.status(404)
                        .send({success: false, message: `${complain.name} not found`})
                        .end();
            })
            .then(() => {
                complain_identities.findAndCountAll({
                    where: {
                        complain_id: complain_id
                    },
                    include: ["identity"]
                })
                    .then(result => {
                        res.send({success: true, data: result})
                            .end();
                    });
            })
            .catch(err => {
                logger.error("complainController:identities, failed to fetch complain identities");
                logger.error(`complainController:identities, ${err}`);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }


    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    async update(req, res) {
        let uid = req.params.id;
        let body = req.body;
        let options = {};
        if (req.body.options !== undefined)
            options = req.body.options;

        complain.findByPk(uid, options)
            .then((result) => {
                if (result === null) {
                    res.status(404)
                        .send({success: false, message: `${complain.name} not found`})
                        .end();
                    return;
                }

                result.update(body)
                    .then(updated => {
                        res.status(200)
                            .send({success: true, data: updated})
                            .end();
                    })
                    .catch(err => {
                        logger.error("complainController:update, failed to update complain");
                        logger.error(`complainController:update, ${err}`);
                        res.status(400)
                            .send({success: false, message: `Request error, ${err}`})
                            .end();
                    });

            })
            .catch(err => {
                logger.error("complainController:update, failed to fetch complain");
                logger.error(`complainController:update, ${err}`);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }

    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    create(req, res) {
        let body = req.body;

        body.object = req.sanitize(body.object);
        body.description = req.sanitize(body.description);

        if (body.object === undefined || body.description === undefined) {
            return res.status(400)
                .json({success: false, message: "Invalid field object or description"})
                .end();
        }

        complain.create({object: body.object, description: body.object, emitter_id: req.uid}).then(complainData => {
            logger.info("complainController:create, new ", complainData.complain_id);

            if (body.complainers !== undefined)
            {
                body.complainers.forEach(async complainer => {
                    if (complainer.identity_id === undefined || complainer.type === undefined)
                    {
                        logger.error("complainController:create, no identity_id or type defined");
                        res.status(400)
                            .json({success: false, message: "Complainer is missing fields (identity_id || type)"})
                            .end();
                        await complainData.destroy();
                        return;
                    }
                    else if (["complainer", "target"].indexOf(complainer.type) === -1)
                    {
                        logger.error("complainController:create, no identity_id or type defined,");
                        res.status(400)
                            .json({success: false, message: "Complainer type is invalid"})
                            .end();
                        await complainData.destroy();
                        return;
                    }

                    await complain_identities.create({...complainer, complain_id: complainData.complain_id});
                });

                return res.status(201)
                    .json({success: true, data: complainData})
                    .end();
            }
        }).catch(err => {
            logger.error("complainController:create, failed to create complain");
            logger.error(`complainController:create, ${err}`);

            return res.status(400)
                .json({success: false, message: err})
                .end();
        });
    }

    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    remove(req, res) {
        complain.findByPk(req.params.id).then(async result => {
            if (result == null) {
                return res.status(404)
                    .json({success: false, message: "Target complain not found"})
                    .end();
            }

            logger.info(`complainController:remove, deleted ${result.complain_id}`);
            await result.destroy();
            res.status(200)
                .json({success: true, data: result})
                .end();
        }).catch(err => {
            logger.error("complainController:remove, failed to fetch complain");
            logger.error(`complainController:remove, ${err}`);
            return res.status(500)
                .json({success: false, message: "Error while trying to fetch complain, " + err})
                .end();
        });

    }

    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    search(req, res) {
        Promise.all([
            complain.findAll({
                where: {
                    [Op.or]: [
                        {
                            object: {
                                [Op.iLike]: `%${req.search.query}%`
                            }
                        },
                        {
                            description: {
                                [Op.iLike]: `%${req.search.query}%`
                            }
                        }
                    ]
                },
                include: ["identities"]
            }
            ),
            complain.findAll({
                include: [{
                    association: "identities",
                    where: {
                        [Op.or]: [
                            {first_name: {
                                [Op.iLike]: `%${req.search.query}%`
                            }},
                            {last_name: {
                                [Op.iLike]: `%${req.search.query}%`
                            }}
                        ]
                    }
                }],
            }
            ),
        ]).then(([complains_source, complains_identities]) => {
            if (complains_source !== null && complains_identities !== null) {
                // Filter values to not have duplicate data
                complains_identities = complains_identities.filter(val => complains_source.find(c => c.complain_id === val.complain_id) === null);
                return complains_source.concat(complains_identities);
            }
            else if (complains_source === null && complains_identities !== null)
                return complains_identities;
            else if (complains_source !== null && complains_identities === null)
                return complains_source;
            else
                return [];
        })
            .then((result) => {
                if (req.search.offset)
                    result = result.slice(req.search.offset);

                if (req.search.limit)
                    result = result.slice(0, req.search.limit);

                res.send({success: true, data: {count: result.length, rows: result}})
                    .end();
            })
            .catch(err => {
                logger.error("complainController:search, failed to search complains");
                logger.error(`complainController:search, ${err}`);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }

}

export default new ComplainController();