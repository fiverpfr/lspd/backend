import logger from "../bin/logger";
import {user} from "../models/user";
import bcrypt from "bcrypt";
import {complain} from "../models/complain";
import {guard} from "../middlewares/guard";
import {rank} from "../models/rank";

class UserController {
    get(req, res) {
        let uid = req.params.id;

        user.findActiveUserByPk(uid, {include: ["ranks"]})
            .then((result) => {
                if (result != null)
                    res.send({success: true, data: result})
                        .end();
                else
                    res.status(404)
                        .send({success: false, message: "User not found"})
                        .end();
            })
            .catch(err => {
                logger.error(err);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }

    getAll(req, res) {
        user.findAll({
            limit: req.search.limit || undefined,
            offset: req.search.offset || 0,
            include: ["ranks"]
        })
            .then(function (result) {
                res.send({success: true, data: {rows: result, count: result.length}})
                    .end();
            })
            .catch(err => {
                logger.error(err);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }

    getAllCount(req, res) {
        user.count()
            .then(function (result) {
                res.send({success: true, data: result})
                    .end();
            })
            .catch(err => {
                logger.error(err);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }

    async setRank(req, res) {
        let target = req.target_user;
        let target_rank = req.body;

        if (!target_rank.rankId || target_rank.primary === undefined)
        {
            res.status(409)
                .json({success: false, message: "Missing primary or rankId fields"})
                .end();
        }

        target_rank.primary = target_rank.primary === "true" || target_rank.primary;

        if (await rank.findByPk(target_rank.rankId) === null)
        {
            res.status(400)
                .json({success: false, message: "Rank not found"})
                .end();
        }
        else if (target.ranks?.find(c => c.rankId === target_rank.rankId) && target.primary_rank.rankId === target_rank.rankId )
        {
            res.status(409)
                .json({success: false, message: "User has already this target_rank and is primary"})
                .end();
        }
        else if (target.ranks?.find(c => c.rankId === target_rank.rankId)?.user_ranks.primary === target_rank.primary )
        {
            res.status(409)
                .json({success: false, message: "User has already this target_rank and is already at the needed primary state"})
                .end();
        }
        else
        {
            let rank_result = await target.addOrUpdateRank(target_rank.rankId, target_rank.primary);
            res.status(200)
                .json({success: true, data: rank_result})
                .end();
        }
    }

    /**
     *
     * @param {Request} req
     * @param {Response} res
     */
    async removeRank(req, res) {
        let target = req.target_user;
        let target_rank = req.target_rank;

        if (!target.ranks?.find(c => c.rankId === target_rank.rankId))
        {
            res.status(400)
                .json({success: false, message: "User doesn't have that rank"})
                .end();
        }
        else if (target.primary_rank.rankId === target_rank.rankId)
        {
            res.status(409)
                .json({success: false, message: "Must change the primary rank before removing it"})
                .end();
        }
        else
        {
            target.ranks.find(c => c.rankId === target_rank.rankId).user_ranks.destroy();
            res.status(200)
                .json({success: true, data: target_rank})
                .end();
        }
    }

    async update(req, res) {
        let body = req.body;

        if (body.password?.length === 0) delete body.password;

        if (body.password && body.password.length > 64)
        {
            res.status(400)
                .json({success: false, message: "New password too long (max 64 characters)"})
                .end();
            return;
        }
        else if (body.password)
            body.password = await bcrypt.hash(body.password, 10);

        if (body.primary_rank)
        {
            let updatedRank = await req.target_user.addOrUpdateRank(body.primary_rank, true);

            if (typeof updatedRank === "string")
            {
                res.status(400)
                    .send({success: false, message: "Failed to update rank: " + updatedRank})
                    .end();
                return;
            }

            delete body.primary_rank;
        }

        req.target_user.update(body)
            .then(updated => {
                updated.password = undefined;
                res.status(200)
                    .send({success: true, data: updated})
                    .end();
            })
            .catch(err => {
                logger.error(err);
                res.status(400)
                    .send({success: false, message: `Request error, ${err}`})
                    .end();
            });
    }

    async create(req, res) {
        let body = req.body;

        if (body.last_name.includes(" ") || body.first_name.includes(" "))
        {
            res.status(400)
                .json({success: false, message: "Last_name and first_name should not have spaces"})
                .end();
            return;
        }

        if (body.password.length > 64)
        {
            res.status(400)
                .json({success: false, message: "New password too long (max 64 characters)"})
                .end();
            return;
        }
        else
            body.password = await bcrypt.hash(body.password, 10);

        user.create(body).then(userData => {
            logger.info("New user created ", userData.username);
            userData.password = undefined;
            return res.status(201)
                .json({success: true, data: userData})
                .end();
        }).catch(err => {
            logger.error("Failed to create new user: ", err);

            return res.status(400)
                .json({success: false, message: err})
                .end();
        });
    }

    me(req, res) {
        user.findActiveUserByPk(req.uid, {include: ["ranks"]}).then(result => {
            if (result == null) {
                return res.status(404)
                    .json({success: false, message: "User not found"})
                    .end();
            }

            res.status(200)
                .json({success: true, data: result})
                .end();
        }).catch(err => {
            return res.status(500)
                .json({success: false, message: `Error while trying to fetch user: ${err}`})
                .end();
        });
    }

    me_complains(req, res) {
        complain.findAll({
            where: {
                emitter_id: req.uid
            },
            order: [["updatedAt", "DESC"]],
            include: ["identities"],
            limit: req.search.limit || 15
        }).then(result => {
            if (result == null)
                result = [];

            res.status(200)
                .json({success: true, data: result})
                .end();
        }).catch(err => {
            return res.status(500)
                .json({success: false, message: `Error while trying to fetch complains of user ${req.uid}: ${err}`})
                .end();
        });
    }

    me_can(req, res) {
        logger.info(`Fetching user permission ${req.query.perm}`);
        guard.requireAny(req.query.perm, "*")(req, res, (err) => {
            if (err?.isGuard) {
                res.status(403)
                    .json({success: false, message: req.query.perm})
                    .end();
            }
            else
            {
                res.status(200)
                    .json({success: true, message: req.query.perm})
                    .end();
            }
        });
    }

    remove(req, res) {
        user.findByPk(req.params.id)
            .then(async result => {
                logger.info(`Deleted user ${result.id}`);
                await result.destroy();
                res.status(200)
                    .json({success: true, data: result})
                    .end();
            }).catch(() => {
                return res.status(500)
                    .json({success: false, message: "Error while trying to fetch user"})
                    .end();
            });

    }
}

export default new UserController();