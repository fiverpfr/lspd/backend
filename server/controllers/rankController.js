import logger from "../bin/logger";
import {rank} from "../models/rank";
import slug from "../utils/slug";

class RankController {
    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    get(req, res) {
        let uid = req.params.id;

        rank.findByPk(uid)
            .then((result) => {
                if (result != null)
                    res.send({success: true, data: result})
                        .end();
                else
                    res.status(404)
                        .send({success: false, message: `${rank.name} not found`})
                        .end();
            })
            .catch(err => {
                logger.error("rankController:get, failed to fetch rank");
                logger.error(`rankController:get, ${err}`);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }

    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    getAll(req, res) {
        rank.findAndCountAll()
            .then(function (result) {
                res.send({success: true, data: result})
                    .end();
            })
            .catch(err => {
                logger.error("rankController:getAll, failed to fetch rank");
                logger.error(`rankController:getAll, ${err}`);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }

    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    async update(req, res) {
        let uid = req.params.id;
        let body = req.body;

        rank.findByPk(uid)
            .then((result) => {
                if (result === null) {
                    res.status(404)
                        .send({success: false, message: `${rank.name} not found`})
                        .end();
                    return;
                }

                result.update(body)
                    .then(updated => {
                        res.status(200)
                            .send({success: true, data: updated})
                            .end();
                    })
                    .catch(err => {
                        logger.error("rankController:update, failed to update rank");
                        logger.error(`rankController:update, ${err}`);
                        res.status(400)
                            .send({success: false, message: `Request error, ${err}`})
                            .end();
                    });

            })
            .catch(err => {
                logger.error("rankController:update, failed to fetch rank");
                logger.error(`rankController:update, ${err}`);
                res.status(500)
                    .send({success: false, message: `Server error, ${err}`})
                    .end();
            });
    }

    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    async create(req, res) {
        let body = req.body;

        if (body.name === undefined) {
            return res.status(400)
                .json({success: false, message: "Need variable 'name'"})
                .end();
        }

        body.rankId = slug(body.name);

        rank.create(body).then(rankData => {
            logger.info(`rankController:create, new ${rankData.name}`);
            return res.status(201)
                .json({success: true, data: rankData})
                .end();
        }).catch(err => {
            logger.error("rankController:create, failed to create rank");
            logger.error(`rankController:create, ${err}`);

            return res.status(400)
                .json({success: false, message: err})
                .end();
        });
    }

    /**
     *
     * @param {Request} req
     * @param {ResponseType} res
     */
    remove(req, res) {
        rank.findByPk(req.params.id).then(async result => {
            if (result == null) {
                return res.status(404)
                    .json({success: false, message: "Target rank not found"})
                    .end();
            }

            logger.info(`rankController:remove, deleted ${result.rankId}`);
            await result.destroy();
            res.status(200)
                .json({success: true, data: result})
                .end();
        }).catch(err => {
            logger.error("rankController:remove, failed to remove rank");
            logger.error(`rankController:remove, ${err}`);
            return res.status(500)
                .json({success: false, message: "Error while trying to fetch rank, " + err})
                .end();
        });

    }
}

export default new RankController();