require("dotenv").config();

/**
 * Configuration used for sequelize-cli, to perform
 * administration tasks
 */
module.exports = {
    "development": {
        "username": process.env.DATABASE_USER,
        "password": process.env.DATABASE_PASS,
        "database": process.env.DATABASE_NAME,
        "host": process.env.DATABASE_HOST,
        "port": parseInt(process.env.DATABASE_PORT),
        "dialect": "postgres"
    },
    "test": {
        "username": process.env.POSTGRESQL_ADDON_USER || process.env.DATABASE_USER,
        "password": process.env.POSTGRESQL_ADDON_PASSWORD || process.env.DATABASE_PASS,
        "database": process.env.POSTGRESQL_ADDON_DB || process.env.DATABASE_NAME,
        "host": process.env.POSTGRESQL_ADDON_HOST || process.env.DATABASE_HOST,
        "port": parseInt(process.env.POSTGRESQL_ADDON_PORT || process.env.DATABASE_PORT),
        "dialect": "postgres"
    },
    "production": {
        "username": process.env.POSTGRESQL_ADDON_USER || process.env.DATABASE_USER,
        "password": process.env.POSTGRESQL_ADDON_PASSWORD || process.env.DATABASE_PASS,
        "database": process.env.POSTGRESQL_ADDON_DB || process.env.DATABASE_NAME,
        "host": process.env.POSTGRESQL_ADDON_HOST || process.env.DATABASE_HOST,
        "port": parseInt(process.env.POSTGRESQL_ADDON_PORT || process.env.DATABASE_PORT),
        "dialect": "postgres"
    }
};
