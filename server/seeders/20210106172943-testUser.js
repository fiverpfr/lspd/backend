"use strict";
require("dotenv").config();

module.exports = {
    up: async (queryInterface) => {
        if (process.env.NODE_ENV !== "development" && process.env.NODE_ENV !== "test")
            return;

        await queryInterface.bulkInsert("users", [{
            social_id: 123456789,
            first_name: "testUser",
            last_name: "testUser",
            password: "$2b$10$1bQpOdXFmxvnbbBKa.4CY.ZtRhNDo.6mzlq2sYSV2xhV31n2Fyl0G",
            active: true,
            createdAt: new Date(),
            updatedAt: new Date()
        }], {});

        await queryInterface.bulkInsert("user_ranks", [
            {
                rankId: "lieutenant",
                userId: 2,
                primary: true,
                createdAt: new Date(),
                updatedAt: new Date()
            }
        ]);

        await queryInterface.bulkInsert("users", [{
            social_id: 123456789,
            first_name: "adminUser",
            last_name: "adminUser",
            password: "$2b$10$nx4kkE7z7hkoewmzthXOZu5yYODAIvaP6IO4Jq.Yk8.7YB3uMuIhm",
            active: true,
            createdAt: new Date(),
            updatedAt: new Date()
        }], {});

        await queryInterface.bulkInsert("user_ranks", [
            {
                rankId: "capitaine",
                userId: 3,
                primary: true,
                createdAt: new Date(),
                updatedAt: new Date()
            }
        ]);

        await queryInterface.bulkInsert("users", [{
            social_id: 123456789,
            first_name: "superAdmin",
            last_name: "superAdmin",
            password: "$2b$10$NEoxI1FY1TkBX2.upwyiCegPDTz.eguE/PyeWcu.07WTi4pIugcSi",
            active: true,
            createdAt: new Date(),
            updatedAt: new Date()
        }], {});

        await queryInterface.bulkInsert("user_ranks", [
            {
                rankId: "frp_staff",
                userId: 4,
                primary: true,
                createdAt: new Date(),
                updatedAt: new Date()
            }
        ]);


    },

    down: async (queryInterface) => {
        if (process.env.NODE_ENV !== "development" && process.env.NODE_ENV !== "test")
            return;

        await queryInterface.bulkDelete("users", {social_id: 123456789}, {});
        await queryInterface.bulkDelete("user_ranks", {userId: 1});
    }
};
