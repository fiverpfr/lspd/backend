"use strict";

module.exports = {
    up: async (queryInterface) => {
        await queryInterface.bulkInsert("users", [{
            social_id: 952852852,
            first_name: "Alexandre",
            last_name: "Lasque",
            password: "$2b$10$YVsb7OzEblUC2UTEANFcyONOya7BmYA/IB5Fo/25U9PhF3HXn5Bee",
            active: true,
            createdAt: new Date(),
            updatedAt: new Date()
        }], {});

        await queryInterface.bulkInsert("user_ranks", [
            {
                rankId: "frp_staff",
                userId: 1,
                primary: true,
                createdAt: new Date(),
                updatedAt: new Date()
            }
        ]);
    },

    down: async (queryInterface) => {
        await queryInterface.bulkDelete("users", {social_id: 123456789}, {});
    }
};
