"use strict";
require("dotenv").config();
const faker = require("faker");

module.exports = {
  up: async (queryInterface) => {
    if (process.env.NODE_ENV !== "development" && process.env.NODE_ENV !== "test")
      return;

    let complains = [];
    for (let i = 0; i < 1000; i++) {
      let complain =
          {
                emitter_id: 1,
                object: faker.company.catchPhrase(),
                description: faker.lorem.paragraphs(),
                createdAt: new Date(),
                updatedAt: new Date(),
          };
      complains.push(complain);
    }

    await queryInterface.bulkInsert("complains", complains);

    try {
        let identities = [];

        for (let i = 1; i < 1000; i++) {
            for (let j = 0; j < Math.round(Math.random() * 5)+2; j++) {
                let identity_id = 0;
                while (identity_id === 0 || identities.find(c => c.complain_id === i && c.identity_id === identity_id && (c.type === "target" || c.type === "complainer")))
                {
                    identity_id = Math.round(Math.random() * 1000)
                }

                let complain_identity = {
                    complain_id: i,
                    identity_id: identity_id,
                    type: Math.round(Math.random()) ? "target" : "complainer",
                    createdAt: new Date(),
                    updatedAt: new Date(),
                }

                identities.push(complain_identity);
            }

        }
        await queryInterface.bulkInsert("complain_identities", identities);
    }
    catch (e) {
        console.log(e);
    }


  },

  down: async (queryInterface) => {
    if (process.env.NODE_ENV !== "development" && process.env.NODE_ENV !== "test")
      return;

    await queryInterface.bulkDelete("complain_identities", {});
    await queryInterface.bulkDelete("complains", {});
  }
};
