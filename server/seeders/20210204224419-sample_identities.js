"use strict";
require("dotenv").config();
const faker = require("faker");

module.exports = {
  up: async (queryInterface) => {
    if (process.env.NODE_ENV !== "development" && process.env.NODE_ENV !== "test")
      return;

    let identities = [];
    for (let i = 0; i < 1000; i++) {
      let identity =
          {
            first_name: faker.name.firstName(),
            last_name: faker.name.lastName(),
            birth_date: faker.date.past(),
            gender: Math.round(Math.random() * 2),
            phone_number: faker.phone.phoneNumberFormat(),
            createdAt: faker.date.past(),
            updatedAt: faker.date.past()
          };

      identities.push(identity);
    }

    await queryInterface.bulkInsert("identities", identities);
  },

  down: async (queryInterface) => {
    if (process.env.NODE_ENV !== "development" && process.env.NODE_ENV !== "test")
      return;

    await queryInterface.bulkDelete("identities", {});
  }
};
