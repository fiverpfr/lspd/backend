"use strict";

module.exports = {
    up: async (queryInterface) => {
        await queryInterface.bulkInsert("ranks", [
            {
                rankId: "frp_staff",
                name: "Staff FiveRP",
                updatedAt: new Date(),
                createdAt: new Date()
            },
            {
                rankId: "capitaine",
                name: "Capitaine",
                updatedAt: new Date(),
                createdAt: new Date()
            },
            {
                rankId: "lieutenant",
                name: "Lieutenant",
                updatedAt: new Date(),
                createdAt: new Date()
            },
            {
                rankId: "sergent",
                name: "Sergent",
                updatedAt: new Date(),
                createdAt: new Date()
            },
            {
                rankId: "inspecteur",
                name: "Inspecteur",
                updatedAt: new Date(),
                createdAt: new Date()
            },
            {
                rankId: "officier",
                name: "Officier",
                updatedAt: new Date(),
                createdAt: new Date()
            }
        ]);
    },

    down: async (queryInterface) => {
        await queryInterface.bulkDelete("ranks", null, {});
    }
};
