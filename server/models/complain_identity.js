import {Model, Sequelize} from "sequelize";

/**
 * @typedef complain_identity
 * @property {integer} complain_id.required - Id of complain
 * @property {integer} identity_id.required - Id of target identity
 * @property {string} type.required - Type of identity in complain:
 */
export class complain_identities extends Model {
    static associate(models) {
        complain_identities.belongsTo(models.identity, {through: "identities", foreignKey: "identity_id", as: "identity"});
        complain_identities.belongsTo(models.complain, {through: "complains", foreignKey: "identity_id", as: "complain"});
    }
}

/**
 * Initialize sequelize model
 * Complain_identities should not be initialized, as it is already by Sequelize
 * in the declaration of belongsToMany.
 *
 * However, as we add a custom column named `type`,
 * we are forced to create a model
 * @param {Sequelize} sequelize instance
 * @returns {Model} user - User model
 */
export const init = (sequelize) => complain_identities.init(
    {
        complain_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            references: {
                model: "complains",
                key: "complain_id"
            },
            onUpdate: "CASCADE",
            allowNull: false
        },
        identity_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            references: {
                model: "identities",
                key: "identity_id"
            },
            onUpdate: "CASCADE",
            allowNull: false
        },
        type: {
            type: Sequelize.ENUM("complainer", "target"),
            allowNull: false
        },
    }, {
        sequelize: sequelize,
        tableName: "complain_identities"
    }
);