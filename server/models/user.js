import {DataTypes, Model, Sequelize} from "sequelize";
import logger from "../bin/logger";
import {rank} from "./rank";
import {user_ranks} from "./user_rank";

/**
 * @typedef user
 * @property {integer} id - user identifier - eg: 8
 * @property {integer} social_id.required - Identifier for Rockstar Social Club - eg: 8865559745258
 * @property {string} first_name.required - First name of character - eg: John
 * @property {string} last_name.required - Last name of character - eg: Doe
 * @property {string} password.required - Password of user
 * @property {boolean} active - Define if account is either active or not
 */
export class user extends Model {
    /**
     * Associations for current model
     * @param {object} models
     */
    static associate(models) {
        /**
         * Alias are used so requests options can be a string name instead of a sequelize model
         */
        user.belongsToMany(models.rank, {through: "user_ranks", foreignKey: "userId",onDelete: "CASCADE", as: "ranks"});
        user.hasMany(models.complain, {foreignKey: "emitter_id", sourceKey: "id", as:"complains"});
    }

    /**
     * override findOne to delete password
     * @param {object} options - passed in Sequelize.findOne
     * @param {boolean} withPassword - Does not delete password from output (needed in Auth case)
     * @returns {Model} user - User Model
     */
    static async findOne(options, withPassword = false) {
        let result = await super.findOne(options);

        if (result === null)
            return null;

        // Delete on result.password isn't working
        // I admit that the object isn't configurable
        if (!withPassword)
            result.password = undefined;

        return result;
    }

    /**
     *
     * @param {object} options - passed in Sequelize.findOne
     * @returns {Model} user - User Model
     */
    static async findActiveUser(options) {
        if (options === null || options === undefined)
            options = {};

        options.where = { ...options.where, active: true};
        options = {...options};
        let userData = null;

        try {
            userData = await user.findOne(options);
        } catch (e) {
            logger.error("Error in findActiveUser, ", e);
        }

        return userData;
    }

    /**
     *
     * @param {object} options - passed in Sequelize.findOne
     * @returns {Model} user - User Model
     */
    static async findActiveUserWithPassword(options) {
        if (options === null || options === undefined)
            options = {};

        options.where = { ...options.where, active: true};
        options = {...options, include: "ranks"};
        let userData = null;

        try {
            userData = await user.findOne(options, true);
        } catch (e) {
            logger.error("Error in findActiveUser, ", e);
        }

        return userData;
    }

    /**
     * Add or update a rank
     * @param {string} rankId - slug for the rank
     * @param {boolean} primary - if the rank is the new primary one
     * @returns {string|Promise}
     */
    async addOrUpdateRank(rankId, primary) {
        if (rankId === null) return "updatePrimaryRank, rankId cannot be null";

        let rankData = await rank.findByPk(rankId, null);
        let targetRank = this.ranks?.find(c => c.rankId === rankId);

        if (rankData === null) return `Rank id ${rankId}, not found`;

        try {
            if (targetRank === undefined && this.primary_rank === undefined)
            {
                return await user_ranks.create({userId: this.id, rankId: rankId, primary: true});
            }
            else if (primary === true)
            {
                let new_rank = await this.replacePrimaryRank(targetRank);
                await this.reload();
                return new_rank;
            }
            else
            {
                return await user_ranks.create({userId: this.id, rankId: rankId, primary: false});
            }
        }
        catch (e) {
            logger.error(e);
            return e.message;
        }
    }

    /**
     * no user should have two primary_rank,
     * this function is here to make the switch
     * @param {user_ranks} targetRank - The new rank
     * @returns {user_ranks} New primary user rank
     */
    async replacePrimaryRank(targetRank) {
        let user_rank = this.primary_rank.get("user_ranks");
        let new_user_rank = targetRank.get("user_ranks");
        user_rank.setPrimary(false);
        new_user_rank.setPrimary(true);

        return new_user_rank;
    }

    /**
     *
     * @param {int} pk - Primary key to find
     * @param {object} options - options for request
     * @returns {Model} user - User model
     */
    static async findActiveUserByPk(pk, options) {
        if (options === null || options === undefined) options = {};

        options.where = { ...options.where, id: pk};
        return this.findActiveUser(options);
    }
}

/**
 * Initialize sequelize model
 * @param {Sequelize} sequelize instance
 * @returns {Model} user - User model
 */
export const init = (sequelize) => user.init({
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        unique: true
    },
    social_id: {
        type: DataTypes.BIGINT,
        allowNull: false
    },
    first_name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            max: 18,
            min: 3
        }
    },
    last_name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            max: 18,
            min: 3
        }
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    username: {
        type: DataTypes.VIRTUAL,
        get() {
            return `${this.first_name}_${this.last_name}`;
        },
        set() {
            logger.warn("Cannot set username as it is a virtual field");
            throw new Error("Value username cannot be set!");
        }
    },
    full_name: {
        type: DataTypes.VIRTUAL,
        get() {
            return `${this.first_name} ${this.last_name}`;
        },
        set() {
            logger.warn("Cannot set full_name as it is a virtual field");
            throw new Error("Value full_name cannot be set");
        }
    },
    primary_rank: {
        type: DataTypes.VIRTUAL,
        get() {
            return this.ranks?.find(c => c.user_ranks.primary === true);
        },
        set() {
            logger.warn("Cannot set primary_rank as it is a virtual field");
            throw new Error("Value primary_rank cannot be set");
        }
    }
}, {
    sequelize: sequelize,
    tableName: "users",
    validate: {
        async nameUnique() {
            let data = await user.findAndCountAll({
                where: {last_name: {[Sequelize.Op.iLike]: this.last_name}, first_name: {[Sequelize.Op.iLike]: this.first_name}}
            });

            // To update his own last_name / first_name, there is one record at least
            // So need to test if this on record is this current or not
            if (data.count > 0 && data.rows.every(id => id.id !== this.id))
            {
                logger.warn("The full username isn't unique, cannot valide");
                throw new Error("Duo last_name & first_name already exists");
            }
        },

        /**
         * In future version, we might check
         * for the unique identifier socialId
         * In order to trace the player
         * @returns {boolean}
         */
        async socialIdExists() {
            return true;
        }
    }
});