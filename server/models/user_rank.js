import {Model, Sequelize} from "sequelize";
import logger from "../bin/logger";

/**
 * @typedef user_rank
 * @property {integer} userId.required - RankId
 * @property {integer} rankId.required - RankId
 * @property {boolean} primary.required - Define if group is the primary one
 */
export class user_ranks extends Model {
    /**
     *
     * @param {boolean} primary - New state to define
     */
    setPrimary(primary = true) {
        this.set("primary", primary);
        this.save()
            .catch(err => {
                logger.error("Failed to save user_rank as primary");
                logger.error(err);
            });
    }
}

/**
 * Initialize sequelize model
 * User_ranks should not be initialized, as it is already by Sequelize
 * in the declaration of belongsToMany.
 *
 * However, as we add a custom column named `primary`,
 * we are forced to create a model
 * @param {Sequelize} sequelize instance
 * @returns {Model} user - User model
 */
export const init = (sequelize) => user_ranks.init(
    {
        createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
        },
        userId: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            references: {
                model: "users",
                key: "id"
            },
            onUpdate: "CASCADE"
        },
        rankId: {
            type: Sequelize.STRING,
            primaryKey: true,
            references: {
                model: "ranks",
                key: "idRank"
            },
            onUpdate: "CASCADE"
        },
        primary: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        }
    }, {
        sequelize: sequelize,
        tableName: "user_ranks"
    }
);