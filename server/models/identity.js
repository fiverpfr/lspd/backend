"use strict";
import {Model, DataTypes, Sequelize} from "sequelize";
import logger from "../bin/logger";

/**
 * @typedef identity
 * @property {integer} identityId.required - ID of identity
 * @property {string} first_name.required - First_name
 * @property {string} last_name.required - Last_name
 * @property {Date} birth_date.required - Birth date of identity (if exists)
 * @property {Boolean} gender.required - 0: male, 1: female, 2: other
 * @property {string} phone_number.required - Format: XXXX-XXXX
 */
export class identity extends Model {
    static associate(models) {
        identity.belongsToMany(models.complain, {through: "complain_identities", foreignKey: "identity_id", as: "complains"});
    }
}

/**
 * Initialize sequelize model
 * @param {Sequelize} sequelize instance
 * @returns {Model} Identity - Identity model
 */
export const init =  (sequelize) => identity.init({
    identity_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        unique: true,
        autoIncrement: true
    },
    first_name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    last_name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    birth_date: {
        type: DataTypes.DATE,
        allowNull: true
    },
    gender: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    phone_number: {
        type: DataTypes.STRING,
        defaultValue: "0000-0000",
        allowNull: false
    },
    full_name: {
        type: DataTypes.VIRTUAL,
        get() {
            return `${this.first_name} ${this.last_name}`;
        },
        set() {
            logger.warn("Cannot set full_name as it is a virtual field");
            throw new Error("Value full_name cannot be set");
        }
    }
}, {
    sequelize: sequelize,
    validate: {
        async nameUnique() {
            let data = await identity.findAndCountAll({
                where: {last_name: {[Sequelize.Op.iLike]: this.last_name}, first_name: {[Sequelize.Op.iLike]: this.first_name}}
            });

            // To update his own last_name / first_name, there is one record at least
            // So need to test if this on record is this current or not
            if (data.count > 0 && data.rows.every(id => id.identity_id !== this.identity_id)) {
                logger.warn("Failed to validate full_name as it is not unique");
                throw new Error("Duo last_name & first_name already exists");
            }

        },

        phoneNumberFormat() {
            let phone_split =  this.phone_number.split("-");
            const formatError = () => {
                logger.warn("Failed to validate identity for ", this.full_name, ", phone_number format not correct, got: ", this.phone_number);
                throw new Error("Phone number format is incorrect, please follow: 1234-1234");
            };

            if (phone_split.length !== 2)
                formatError();

            if (this.phone_number.length !== 9)
                formatError();
        },

        birthdateValid() {
            // Required age on project is 18 yo
            let requiredYear = (new Date()).getFullYear()-18;

            if (new Date(this.birth_date).getFullYear() >= requiredYear)
            {
                logger.warn("Failed to validate birthDate");
                throw new Error("Birthdate is invalid, cannot be above " + requiredYear);
            }
        },

        genderValid() {
            if (this.gender > 2 || this.gender < 0)
            {
                logger.warn("Failed to validate gender, must be below 2 and above 0");
                throw new Error("Gender is invalid, cannot be above 2 and below 0");
            }
        }
    }
});
