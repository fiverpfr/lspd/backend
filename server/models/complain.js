"use strict";
import {Model, DataTypes} from "sequelize";

/**
 * @typedef complain
 * @property {integer} complain_id.required - ID of complain
 * @property {string} emitter_id.required - Id of user emitter
 * @property {string} object.required - Object of complain
 * @property {string} description.required - Description of complain
 */
export class complain extends Model {
    static associate(models) {
        complain.belongsToMany(models.identity, {through: "complain_identities", foreignKey: "complain_id", as: "identities"});
        complain.hasOne(models.user, {foreignKey: "id", sourceKey: "emitter_id", as: "emitter"});
    }
}

/**
 * Initialize sequelize model
 * @param {Sequelize} sequelize instance
 * @returns {Model} Identity - Identity model
 */
export const init =  (sequelize) => complain.init({
    complain_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    emitter_id: {
        type: DataTypes.STRING,
        allowNull: false,
        references: {
            model: "users",
            key: "id"
        }
    },
    object: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: false
    }
}, {
    sequelize: sequelize
});
