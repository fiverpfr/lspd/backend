"use strict";
import {Model, DataTypes} from "sequelize";

/**
 * @typedef rank
 * @property {integer} idRank.required - RankId
 * @property {string} name.required - Rank name
 */
export class rank extends Model {
    static associate(models) {
        rank.belongsToMany(models.user, {through: "user_ranks", foreignKey: "rankId", as: "users"});
    }
}

/**
 * Initialize sequelize model
 * @param {Sequelize} sequelize instance
 * @returns {Model} Rank - Rank model
 */
export const init = (sequelize) => rank.init({
    rankId:  {
        type: DataTypes.STRING,
        primaryKey: true,
        unique: true
    },
    name: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false
    }
}, {
    sequelize: sequelize,
    tableName: "ranks"
}
);