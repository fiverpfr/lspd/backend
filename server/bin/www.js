#!/usr/bin/env node
"use strict";
import "dotenv/config"; // Import ENV variables in .env
import http from "http";
import app from "../app";
import logger from "./logger";
import sequelize from "../sequelize";

/**
 * Get port from environment and store in Express.
 */
const port = normalizePort(process.env.PORT || "3001");
app.set("port", port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);
server.listen(port);
server.on("error", onError);
server.on("listening", onListening);

/**
 * Process events
 */
process.on("SIGTERM", gracefulStop);
process.on("SIGINT", gracefulStop);

/**
 * Gracefull stop the service
 */
function gracefulStop() {
    logger.info("Stop signal received [SIGTERM, SIGINT]");
    logger.info("Stopping HTTP server...");
    server.close(async () => {
        logger.info("HTTP server stopped");
        logger.info("Stopping Sequelize connection...");
        await sequelize.close()
            .then(() => {
                logger.info("Sequelize connection stopped");
            });
    });
}

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    let port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== "listen") {
        throw error;
    }

    let bind = typeof port === "string"
        ? "Pipe " + port
        : "Port " + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
    case "EACCES":
        logger.error(bind + "requires elevated privileges");
        process.exit(1);
        break;
    case "EADDRINUSE":
        logger.error(bind + " is already in use");
        process.exit(1);
        break;
    default:
        throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    let addr = server.address();
    let bind = typeof addr === "string"
        ? "pipe " + addr
        : "port " + addr.port;
    logger.info("Listening on " + bind);
}