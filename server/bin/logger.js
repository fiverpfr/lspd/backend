import winstonLib from "winston";
import "dotenv/config";

const logFormat = winstonLib.format.printf(({label, level, message, timestamp}) => {
    return `${timestamp} [${label}] ${level}: ${message}`;
});

const logger = winstonLib.createLogger({
    level: process.env.LOG_LEVEL,
    format: winstonLib.format.combine(
        winstonLib.format.timestamp(),
        winstonLib.format.label({label: "backend"}),
        winstonLib.format.ms(),
        logFormat
    ),
    transports: [
        new winstonLib.transports.Console()
    ]
});

export default logger;