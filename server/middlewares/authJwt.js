import "dotenv/config";
import jwt from "jsonwebtoken";
import logger from "../bin/logger";
import sequelize from "../sequelize";

export function verifyToken(req, res, next) {
    if (!req.headers.authorization) {
        logger.debug("Middleware:verifyToken failed, no authorization ");
        return res
            .status(401)
            .json({success: false, message: "No token provided"})
            .end();
    }

    let token = req.headers.authorization.slice(7);

    jwt.verify(token, process.env.JWT_SECRET, {}, (err, result) => {
        if (err) {
            logger.debug(`Middleware:verifyToken failed with authorization: ${JSON.stringify(req.headers.authorization)}`);

            return res
                .status(401)
                .json({success: false, message: `Invalid token: ${err.name}, ${err.message}`})
                .end();
        }

        req.uid = result.uid;
        next();
    });
}

export function validateLoginFields(req, res, next) {
    const invalidFields = () => {
        logger.warn("Middleware:validateLoginFields, cannot login without the appropriate fields");
        res.status(400)
            .json({success:false, message: "Missing or inappropriate fields, expecting username and password"})
            .end();
    }

    if (req.body === {})
        return invalidFields();

    if (req.body.username === undefined || req.body.username.length === 0 || !req.body.username.includes("_") || req.body.username.includes(" "))
        return invalidFields();

    if (req.body.password === undefined || req.body.password.length === 0)
        return invalidFields();

    if (req.body.username.split("_").length === 1)
        return invalidFields();

    next();
}

export function isActiveUser(req, res, next) {
    sequelize.models.user.findActiveUserByPk(req.uid, {include: ["ranks"]})
        .then(user => {
            if (user === null)
            {
                logger.warn("Middleware:isActiveUser, cannot use his token if user isn't active");
                return res.status(401)
                    .json({success: false, message: "User is either non-existant or not active"})
                    .end();
            }

            req.user = user;
            next();
        })
        .catch(err => {
            logger.error("Failed to fetch activeUser in isActiveUser middleware: " + err);
            res.status(500)
                .json({success: false, message: "Failed to verify user identity"})
                .end();
        });
}