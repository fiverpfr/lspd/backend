import logger from "../bin/logger";
import sequelize from "../sequelize";

export function hasAnyRank(req, res, next) {
    if (req.user.ranks === undefined || req.user.ranks.length === 0)
    {
        logger.error("Middleware:hasAnyRank, user doesn't have any rank");
        res.status(401)
            .send({success: false, message: "User doesn't have any rank"})
            .end();
        return;
    }

    next();
}

export function targetRankExists(req, res, next) {
    sequelize.models.rank.findByPk(req.params.rankId)
        .then(rank => {
            if (rank === null)
            {
                logger.warn("Middleware:targetRankExists, target rank provided is not a valid one");
                res.status(401)
                    .json({success: false, message: "Rank does not exist"})
                    .end();
                return;
            }

            req.target_rank = rank;
            next();
        })
        .catch(err => {
            logger.error("Middleware:targetRankExists, failed to fetch target rank: " + err);
            logger.error("Middleware:targetRankExists " + err);
            res.status(500)
                .json({success: false, message: "Failed to verify rank"})
                .end();
        });
}