import {applicationHealthy} from "../utils/health";
import logger from "../bin/logger";

export function isApplicationReady(req, res, next) {
    if (!applicationHealthy().healthy)
    {
        logger.info("Trying to use application while it is not ready");
        res.status(503)
            .json({success: false, message: "Application not ready"})
            .end();
        return;
    }

    next();
}