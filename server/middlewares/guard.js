import Guard from "express-guard";

const admin = new Guard.Role("admin", {
    can: ["*"],
    func: async (req) => {
        return req.user.ranks.find(c => c.rankId === "frp_staff") !== undefined;
    }
});

const capitaine = new Guard.Role("capitaine", {
    can: [
        "user.list", "user.update", "user.create", "user.toggle",
        "rank.create", "rank.update", "rank.read",
        "user_rank.create", "user_rank.delete",
        "complain.delete",
        "identity.delete"
    ],
    func: async (req) => {
        return req.user.ranks.find(c => c.rankId === "capitaine") !== undefined;
    }
});

const lieutenant = new Guard.Role("lieutenant", {
    can: [
        "user.list"
    ],
    func: async (req) => {
        return req.user.ranks.find(c => c.rankId === "lieutenant") !== undefined;
    }
});

const guard = new Guard();

guard.roles.addRole(admin);
guard.roles.addRole(capitaine);
guard.roles.addRole(lieutenant);

export {guard};