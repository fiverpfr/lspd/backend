import logger from "../bin/logger";

export default function parseSearchParams(req, res, next) {
    let query = decodeURI(req.query.q);
    let limit = parseInt(req.query.limit);
    let offset = parseInt(req.query.offset);

    if (req.query.limit !== undefined && isNaN(limit) || req.query.offset !== undefined && isNaN(offset))
    {
        logger.warn("Middleware:parseSearchParams, limit and/or offset params are invalid, expected numbers");
        logger.warn(`Middleware:parseSearchParams, limit: ${req.query.limit}, offset: ${req.query.offset}`);
        res.status(400)
            .send({success: false, message: "Limit and offset must be numbers"})
            .end();
        return;
    }

    req.search = {
        query: query,
        limit: limit,
        offset: offset
    };
    next();
}

export function parsePermission(req, res, next) {
    if (!req.query.perm)
    {
        logger.warn("Middleware:parsePermission, cannot retrieve permission if none provided");
        res.status(400)
            .json({success: false, message: "No permission provided"})
            .end();
        return;
    }

    next();
}