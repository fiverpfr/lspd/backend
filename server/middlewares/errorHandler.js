import logger from "../bin/logger";

const errorHandler = (err, req, res, next) => {
    if (err.isGuard) {
        logger.warn(`User cannot access the requested resource, uid: ${req.uid}`);
        res.status(403)
            .send({success: false, message: "User doesn't have access to this specific resource"})
            .end();
    }
    else {
        res.status(400)
            .send({success: false, message: "Received data are incorrect"})
            .end();
    }

    next(err);
};

export {errorHandler};