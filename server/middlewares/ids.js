import sequelize from "../sequelize";
import logger from "../bin/logger";

export function isRequestIdValid(req, res, next) {
    if (isNaN(req.params.id) || req.params.id < 0)
    {
        logger.warn(`Middleware:isRequestIdValid, user provided an invalid id, id: ${req.uid}, requested id: ${req.params.id}`);
        res.status(400)
            .json({success: false, message: "Provided id is invalid"})
            .end();
    }
    else
        next();
}

export function targetUserExists(req, res, next) {
    sequelize.models.user.findActiveUserByPk(req.params.id, {include: ["ranks"]})
        .then(user => {
            if (user === null)
            {
                logger.warn("Middleware:targetUserExists, target user provided is not a valid one");
                res.status(401)
                    .json({success: false, message: "User is either non-existant or not active"})
                    .end();
                return;
            }

            req.target_user = user;
            next();
        })
        .catch(err => {
            logger.error("Middleware:targetUserExists, failed to fetch activeUser" + err);
            logger.error("Middleware:targetUserExists " + err);
            res.status(500)
                .json({success: false, message: "Failed to verify user identity"})
                .end();
        });
}

export function targetUserExistsAndInactive(req, res, next) {
    sequelize.models.user.findByPk(req.params.id, {include: ["ranks"]})
        .then(user => {
            if (user === null)
            {
                logger.warn("Middleware:targetUserExistsAndInactive, target user provided is not a valid one");
                res.status(401)
                    .json({success: false, message: "User is either non-existant or not active"})
                    .end();
                return;
            }

            req.target_user = user;
            next();
        })
        .catch(err => {
            logger.error("Middleware:targetUserExistsAndInactive, failed to fetch activeUser");
            logger.error("Middleware:targetUserExistsAndInactive " + err);
            res.status(500)
                .json({success: false, message: "Failed to verify user identity"})
                .end();
        });
}