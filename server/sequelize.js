import {Sequelize} from "sequelize";
import "dotenv/config";
import logger from "./bin/logger";
import * as fs from "fs";
import path from "path";
import dbServers from "./config/config";

const _modelDir = __dirname + "/models";
const credentials = dbServers[process.env.NODE_ENV];
const sequelize = new Sequelize(credentials.database,
    credentials.username,
    credentials.password,
    {
        dialect: credentials.dialect,
        port: credentials.port,
        host: credentials.host,
        omitNull: true,
        pool: {
            max: 1, // Clever cloud free PostgreSQL limit connections to 5
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    });

sequelize.connected = false;

function authenticate() {
    return new Promise((res, rej) => {
        sequelize.authenticate()
            .then(() => res())
            .catch(err => rej(err));
    });
}

function loadModels() {
    return new Promise((resolve, reject) => {
        fs
            .readdir(_modelDir, (err, files) => {
                if (err !== null)
                    reject(err);

                files
                    .filter(file => file.indexOf(".") !== 0 && (file.slice(-3) === ".js"))
                    .forEach(async (file, index) => {
                        let model = await import(path.join(_modelDir, file));
                        try {
                            await model.init(sequelize);
                            logger.debug("Loaded Model " + file);
                        }
                        catch(e) {
                            reject(new Error(e));
                        }

                        if (index === files.length -1) resolve();
                    });
            });
    });
}

function setAssociations() {
    return new Promise((res) => {
        Object.keys(sequelize.models)
            .forEach(modelName => {
                if (sequelize.models[modelName].associate)
                    sequelize.models[modelName].associate(sequelize.models);
            });
        res();
    });
}

// Hm...
authenticate()
    .then(() => loadModels()
        .then(() => setAssociations()
            .then(() => {
                sequelize.connected = true;
                logger.info("Database is up and running");
                logger.info("Application is ready");
            })
        )
    )
    .catch(err => {
        logger.error("Connection to database failed");
        logger.error(err.message);
    });

export default sequelize;