<div align="center">
    <img width="200" src="https://five-roleplay.fr/wp-content/uploads/2020/01/favicon.png" alt="FiveRP Logo">
    <h1 align="center">FiveRP LSPD</h1>
    <p align="center">A NodeJS API to handle LSPD frontend calls</p>
</div>

[![pipeline status](https://gitlab.com/fiverpfr/lspd/backend/badges/master/pipeline.svg)](https://gitlab.com/fiverpfr/lspd/backend/-/commits/master)

## Requirements

* A database server (default: `PostgreSQL`)
* NodeJS installed (tested on: `14.15.3`)
* Yarn

## Local installation

This step-by-step installation is intended for local installation.

### Installing packages

We use `yarn` package managment CLI to install package, if you don't have it see [here](https://classic.yarnpkg.com/en/docs/install/#debian-stable). Then install the package:

```
yarn install
```

### Environment variables

Firstly, you need to fill all the required environment variables so the API is working properly. To do so copy `.env.example` into `.env`. You need at least to fill the database variables so the API can run. **Don't touch the NODE_ENV variable**.

### Creating the database

You will now need to **generate** your database, thanksfully, there is some tool to do that by some simple commands:

```
cd $MY_PROJECT
./node_modules/.bin/sequelize-cli db:migrate
```

### Sample data

Some sample data are available to fill your database with example accounts, run the following command to generate them:

``` 
./node_modules/.bin/sequelize-cli db:seed:all
```

By running the above command you will generate multiple accounts, each have permissions and ranks, the credentials are below.

```
DEMO ACCOUNT 1:
Rank: lieutenant
Username: testUser_testUser
Password: testUser

DEMO ACCOUNT 2:
Rank: capitaine
Username: adminUser_adminUser
Password: adminUser

DEMO ACCOUNT 3:
Rank: frp_staff (highest rank)
Username: superAdmin_superAdmin
Password: superAdmin
```

*If you generate new accounts, be aware: username format needs to always be `FirstName_LastName`*

### Running

Just do the command `npm run nodemon`, and have fun with you API.
